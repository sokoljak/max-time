package time

import "testing"

func TestMaxTime(t *testing.T) {
	tests := []struct{
		input [4]int
		expected string
	}{
		{
			[4]int{3, 1, 2, 7},
			"23:17",
		},
		{
			[4]int{1, 5, 2, 7},
			"21:57",
		},
		{
			[4]int{0, 8, 3, 7},
			"08:37",
		},
		{
			[4]int{9, 0, 0, 0},
			"09:00",
		},
		{
			[4]int{1, 1, 1, 1},
			"11:11",
		},
		{
			[4]int{},
			"00:00",
		},
		{
			[4]int{6, 7, 7, 1},
			"-1",
		},
		{
			[4]int{9, 9, 9, 9},
			"-1",
		},
		{
			[4]int{2, 9, 9, 9},
			"-1",
		},
		{
			[4]int{2, 9, 9},
			"-1",
		},
	}

	for _, tc := range tests {
		got := MaxTime(tc.input)
		if got != tc.expected {
			t.Errorf("MaxTime() = %v, expected %v", got, tc.expected)
		}
	}
}