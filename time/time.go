package time

import "strconv"

/**
	Calculate max time in format HH:MM based on four digit input
	return max time value or -1 if correct date cannot be constructed
 */
func MaxTime(input [4]int) string {
	flag := false
	time := ""
	freq := count(input)

	for k := 2; k >= 0; k-- {
		if hasDigit(freq, k) {
			time = time + strconv.Itoa(k)
			flag = true
			break
		}
	}

	if !flag {
		return "-1"
	}

	flag = false
	if time == "2" {
		for k := 3; k >= 0; k-- {
			if hasDigit(freq, k) {
				time = time + strconv.Itoa(k)
				flag = true
				break
			}
		}
	} else {
		for k := 9; k >= 0; k-- {
			if hasDigit(freq, k) {
				time = time + strconv.Itoa(k)
				flag = true
				break
			}
		}
	}

	if !flag {
		return "-1"
	}

	flag = false
	time = time + ":"
	for k := 5; k >= 0; k-- {
		if hasDigit(freq, k) {
			time = time + strconv.Itoa(k)
			flag = true
			break
		}
	}

	if !flag {
		return "-1"
	}

	for k := 9; k >= 0; k-- {
		if hasDigit(freq, k) {
			time = time + strconv.Itoa(k)
			break
		}
	}

	return time
}

func count(input [4]int) map[int]int {
	freq := make(map[int]int)
	for _, v := range input {
		c := freq[v]

		if c == 0 {
			freq[v] = 1
		}

		freq[v] = c + 1
	}
	return freq
}

func hasDigit(freq map[int]int, d int) bool {
	if c := freq[d]; c > 0 {
		freq[d] = c - 1
		return true
	}

	return false
}