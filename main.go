package main

import (
	"errors"
	"fmt"
	"max-time/time"
)

func main() {

	fmt.Println("Max time calculator!")
	fmt.Println("Give Your input:")

	input, err := readInput()

	if err != nil {
		panic(err)
	}

	maxTime := time.MaxTime(input)
	fmt.Println("Result: ", maxTime)
}


func readInput() ([4]int, error) {
	var i, j, k, l int
	_, err := fmt.Scanf("%d %d %d %d", &i, &j, &k, &l)

	if err != nil {
		return [4]int{}, errors.New("read input fails")
	}

	return [4]int{i, j, k, l}, nil
}